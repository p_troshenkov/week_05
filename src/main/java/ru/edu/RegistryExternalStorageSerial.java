package ru.edu;

import ru.edu.model.Catalog;
import ru.edu.model.Registry;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class RegistryExternalStorageSerial implements RegistryExternalStorage {

    /**
     * Объект Registry.
     */
    private Registry registry;

    /**
     * Конструктор по умолчанию.
     */
    public RegistryExternalStorageSerial() {
    }

    /**
     * Конструктор из Registry.
     *
     * @param registryIn
     */
    public RegistryExternalStorageSerial(final Registry registryIn) {
        registry = registryIn;
    }

    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public void readFrom(final String filePath) {
        try (InputStream fs = new FileInputStream(filePath);
             ObjectInputStream os = new ObjectInputStream(fs)) {
            Catalog catalog = (Catalog) os.readObject();
            registry = catalog.toRegistry();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     */
    @Override
    public void writeTo(final String filePath) {
        try (FileOutputStream fs = new FileOutputStream(filePath);
             ObjectOutputStream os = new ObjectOutputStream(fs)) {
            os.writeObject(registry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
