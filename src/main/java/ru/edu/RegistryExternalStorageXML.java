package ru.edu;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.edu.model.Catalog;
import ru.edu.model.Registry;

import java.io.File;


public class RegistryExternalStorageXML implements RegistryExternalStorage {

    /**
     * Объект Registry.
     */
    private Registry registry;

    /**
     * Конструктор по умолчанию.
     */
    public RegistryExternalStorageXML() {
    }

    /**
     * Конструктор из Registry.
     *
     * @param registryIn
     */
    public RegistryExternalStorageXML(final Registry registryIn) {
        registry = registryIn;
    }
    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public void readFrom(final String filePath) {
        ObjectMapper mapper = new XmlMapper();
        mapper.configure(
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                false
        );
        try {
            registry = mapper
                    .readValue(new File(filePath), Catalog.class)
                    .toRegistry();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     *
     */
    @Override
    public void writeTo(final String filePath) {
        ObjectMapper mapper = new XmlMapper();
        try {
            mapper.writeValue(new File(filePath), registry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
