package ru.edu;


import com.fasterxml.jackson.databind.ObjectMapper;
import ru.edu.model.Catalog;
import ru.edu.model.Registry;

import java.io.File;


public class RegistryExternalStorageJSON implements RegistryExternalStorage {

    /**
     * Объект Registry.
     */
    private Registry registry;

    /**
     * Конструктор по умолчанию.
     */
    public RegistryExternalStorageJSON() {
    }

    /**
     * Конструктор из Registry.
     *
     * @param registryIn
     */
    public RegistryExternalStorageJSON(final Registry registryIn) {
        registry = registryIn;
    }
    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public void readFrom(final String filePath) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            registry = mapper
                    .readValue(new File(filePath), Catalog.class)
                    .toRegistry();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     */
    @Override
    public void writeTo(final String filePath) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File(filePath), registry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
