package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;

public class CD implements Serializable {

    /**
     * Название диска.
     */
    @JsonProperty("title")
    @JacksonXmlProperty(localName = "TITLE")
    private String title;

    /**
     * Имя исполнителя.
     */
    @JsonProperty("artist")
    @JacksonXmlProperty(localName = "ARTIST")
    private String artist;

    /**
     * Название страны.
     */
    @JsonProperty("country")
    @JacksonXmlProperty(localName = "COUNTRY")
    private String country;

    /**
     * Название компании.
     */
    @JsonProperty("company")
    @JacksonXmlProperty(localName = "COMPANY")
    private String company;

    /**
     * Цена.
     */
    @JsonProperty("price")
    @JacksonXmlProperty(localName = "PRICE")
    private double price;

    /**
     * Год выхода.
     */
    @JsonProperty("year")
    @JacksonXmlProperty(localName = "YEAR")
    private int year;

    /**
     * Конструктор по умолчанию.
     */
    public CD() {
    }

    /**
     * Геттер имени исполнителя.
     * @return artist
     */
    public String getArtist() {
        return artist;
    }

    /**
     * Геттер названия страны.
     * @return country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Геттер названия.
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Геттер года выхода.
     * @return year
     */
    public int getYear() {
        return year;
    }
}
