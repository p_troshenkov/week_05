package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Country implements Serializable {

    /**
     * Название страны.
     */
    @JsonProperty("name")
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    /**
     * Список исполнителей страны.
     */
    @JsonProperty("Artist")
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Artist")
    private List<Artist> artists = new ArrayList<>();

    /**
     * Конструктор по умолчанию.
     */
    public Country() {
    }

    /**
     * Конструктор класса стран.
     * @param countryName Название страны
     * @param artistsList Список исполнителей страны
     */
    public Country(final String countryName, final List<Artist> artistsList) {
        name = countryName;
        artists = artistsList;
    }
}
