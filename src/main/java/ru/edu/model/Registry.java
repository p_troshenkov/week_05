package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
@JsonRootName(value = "ArtistRegistry")
@JacksonXmlRootElement(localName = "ArtistRegistry")
public class Registry implements Serializable {

    /**
     * Количество стран.
     */
    @JsonProperty("countryCount")
    @JacksonXmlProperty(localName = "countryCount", isAttribute = true)
    private int countryCount;

    /**
     * Список стран.
     */
    @JsonProperty("Country")
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Country")
    private List<Country> countries = new ArrayList<>();

    /**
     * Конструктор по умолчанию.
     */
    public Registry() {
    }

    /**
     * Конструктор из списка стран.
     *
     * @param countryList
     */
    public Registry(final List<Country> countryList) {
        countries = countryList;
        countryCount = countryList.size();
    }
}
