package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;

public class Album implements Serializable {

    /**
     * Название альбома.
     */
    @JsonProperty("name")
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    /**
     * Год выхода.
     */
    @JsonProperty("year")
    @JacksonXmlProperty(localName = "year", isAttribute = true)
    private long year;

    /**
     * Конструктор из объекта CD.
     *
     * @param cd диск
     */
    public Album(final CD cd) {
        name = cd.getTitle();
        year = cd.getYear();
    }

    /**
     * Конструктор по умолчанию.
     */
    public Album() {
    }
}
