package ru.edu.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@JacksonXmlRootElement(localName = "CATALOG")
public class Catalog implements Serializable {
    /**
     * Список CD.
     */
    @JsonProperty("Catalog")
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "CD")
    private List<CD> cdList = new ArrayList<>();

    /**
     * Конструктор по умолчанию.
     */
    public Catalog() {
    }

    /**
     * Геттер списка CD.
     * @return список CD
     */
    public List<CD> getCdList() {
        return cdList;
    }

    /**
     * Преобразование каталога в объект Registry.
     *
     * @return объект Registry
     */
    public Registry toRegistry() {
        List<Country> countryList = this.getCdList().stream()
                .collect(
                        Collectors.groupingBy(
                                CD::getCountry,
                                Collectors.groupingBy(CD::getArtist)
                        )
                ).values().stream()
                .map(this::addCountries)
                .collect(Collectors.toList());

        return new Registry(countryList);
    }

    /**
     * Преобразование списка дисков в список альбомов.
     * @param cdListIn
     * @return список альбомов
     */
    private List<Album> cdToAlbum(final List<CD> cdListIn) {
        List<Album> cdToAlbum = new ArrayList<>();
        for (CD cd : cdListIn) {
            cdToAlbum.add(new Album(cd));
        }
        return cdToAlbum;
    }

    /**
     * Выборка стран.
     * @param map
     * @return Country
     */
    private Country addCountries(final Map<String, List<CD>> map) {
        String countryName = "";
        List<Artist> artistList = new ArrayList<>();

        for (Map.Entry<String, List<CD>> entry : map.entrySet()) {
            artistList.add(
                    new Artist(
                            entry.getKey(),
                            cdToAlbum(entry.getValue())
                    ));
            countryName = entry.getValue().get(0).getCountry();
        }
        return new Country(countryName, artistList);
    }

}
