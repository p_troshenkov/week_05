package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Artist implements Serializable {

    /**
     * Имя исполнителя.
     */
    @JsonProperty("name")
    @JacksonXmlProperty(localName = "Name")
    private String name;

    /**
     * Список альбомов исполнителя.
     */
    @JsonProperty("Album")
    @JacksonXmlElementWrapper(localName = "Albums")
    @JacksonXmlProperty(localName = "Album")
    private List<Album> albums = new ArrayList<>();

    /**
     * Конструктор объекта исполнителя.
     * @param artistName имя исполнителя
     * @param albumsList список альбомов
     */
    public Artist(final String artistName, final List<Album> albumsList) {
        name = artistName;
        albums = albumsList;
    }

    /**
     * Конструктор по умолчанию.
     */
    public Artist() {
    }
}
