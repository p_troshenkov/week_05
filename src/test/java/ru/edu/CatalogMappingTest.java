package ru.edu;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import org.junit.Test;

import ru.edu.model.*;

import java.io.File;

public class CatalogMappingTest {

    @Test
    public void test() throws Exception {


        ObjectMapper mapper = new XmlMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Catalog catalog = mapper.readValue(new File("./input/cd_catalog.xml"), Catalog.class);

        Registry registry = catalog.toRegistry();

        RegistryExternalStorageSerial regSerial = new RegistryExternalStorageSerial(registry);
        RegistryExternalStorageXML regXml = new RegistryExternalStorageXML(registry);
        RegistryExternalStorageJSON regJson = new RegistryExternalStorageJSON(registry);

        regSerial.writeTo("output/artist_by_country.serialized");
        regXml.writeTo("./output/artist_by_country.xml");
        regJson.writeTo("./output/artist_by_country.json");

    }
}