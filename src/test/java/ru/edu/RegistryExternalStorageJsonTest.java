package ru.edu;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class RegistryExternalStorageJsonTest {
    RegistryExternalStorageJSON regJson = new RegistryExternalStorageJSON();
    String inputFilePath = "./src/test/resources/exampleInput/example_input.json";
    String outputFilePath = "./src/test/resources/exampleOutput/example_output.json";

    @Test
    public void writeTo() {
        regJson.readFrom(inputFilePath);
        regJson.writeTo(outputFilePath);
    }


}