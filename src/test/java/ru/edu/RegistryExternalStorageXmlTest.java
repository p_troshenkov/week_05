package ru.edu;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.junit.Assert.*;

public class RegistryExternalStorageXmlTest {
    RegistryExternalStorageXML regXml = new RegistryExternalStorageXML();
    String inputFilePath = "./src/test/resources/exampleInput/example_input.xml";
    String outputFilePath = "./src/test/resources/exampleOutput/example_output.xml";

    @Test
    public void writeTo() {
        regXml.readFrom(inputFilePath);
        regXml.writeTo(outputFilePath);
    }
}