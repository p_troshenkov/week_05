package ru.edu;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class RegistryExternalStorageSerialTest {
    RegistryExternalStorageSerial regSerial = new RegistryExternalStorageSerial();
    String inputFilePath = "./src/test/resources/exampleInput/example_input.serialized";
    String outputFilePath = "./src/test/resources/exampleOutput/example_output.serialized";

    @Test
    public void writeTo() {
        regSerial.readFrom(inputFilePath);
        regSerial.writeTo(outputFilePath);
    }

    @Test
    public void readFromEx() {

        try {
            regSerial.readFrom(inputFilePath);
            regSerial.writeTo(outputFilePath);
            regSerial.readFrom(outputFilePath);

        } catch (ClassCastException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
    }
}