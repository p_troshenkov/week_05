package ru.edu;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;
import ru.edu.model.Catalog;
import ru.edu.model.Registry;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ExampleCatalogSet {

    @Test
    public void test() throws Exception {


        ObjectMapper mapper = new XmlMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Catalog catalog = mapper.readValue(new File("./input/cd_catalog.xml"), Catalog.class);

        ObjectMapper mapperJson = new ObjectMapper();
        ObjectMapper mapperXml = new XmlMapper();
        mapperXml.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);

        try {
            mapperJson.writeValue(new File("./src/test/resources/exampleInput/example_input.json"), catalog);
            mapperXml.writeValue(new File("./src/test/resources/exampleInput/example_input.xml"), catalog);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileOutputStream fs = new FileOutputStream("./src/test/resources/exampleInput/example_input.serialized");
             ObjectOutputStream os = new ObjectOutputStream(fs)) {
            os.writeObject(catalog);
        } catch (Exception e) {
            e.printStackTrace();
        }




    }
}
